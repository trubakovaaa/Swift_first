import Foundation

// Task 1
func makeBuffer() -> (String) -> Void {
    var storage = ""
    
    return { stringValue in
        if stringValue.isEmpty {
            print(storage)
        } else {
            storage += stringValue
        }
    }
}

var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")

// Task 2
func checkPrimeNumber(_ number: Int) -> Bool {
    if number < 2 {
        return false
    } else if number == 2 || number == 3 {
        return true
    } else {
        for i in 2...number / 2 {
            if number % i == 0 {
                return false
            }
        }
        return true
    }
}

print(checkPrimeNumber(0))
print(checkPrimeNumber(1))
print(checkPrimeNumber(2))
print(checkPrimeNumber(3))
print(checkPrimeNumber(4))
print(checkPrimeNumber(8))
print(checkPrimeNumber(13))
