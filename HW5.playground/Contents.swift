import Foundation

// Homework 5.11.1

open class Shape {
    open func calculateArea() -> Double {
        fatalError("not implemented")
    }
    
    open func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(_ height: Double, _ width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        (height + width) * 2
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(_ radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        2 * Double.pi * radius
    }
}

class Square: Shape {
    private let side: Double
    
    init(_ side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        side * side
    }
    
    override func calculatePerimeter() -> Double {
        4 * side
    }
}

let shapes:[Shape] = [Rectangle(4.0, 3.0), Circle(1.0), Square(2.0)]

var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}

print("Суммарная площадь фигур равна: \(area)")
print("Суммарный периметр фигур равен: \(perimeter)")

// Task 5.11.2

func findIndex(ofString valueToFind: String, in array: [String]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

func findIndexWithGenerics<T: Equatable>(ofElements valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndex = findIndexWithGenerics(ofElements: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndex)")
}

let arrayOfDouble = [-1.3, 2.9, 8.4, 5.0, -2.5, 3.75]
if let foundIndex = findIndexWithGenerics(ofElements: 8.4, in: arrayOfDouble) {
    print("Индекс числа 8.4: \(foundIndex)")
}

let arrayOfInt = [1, 9, 4, 5, 2, 15]
if let foundIndex = findIndexWithGenerics(ofElements: 15, in: arrayOfInt) {
    print("Индекс числа 15: \(foundIndex)")
}
