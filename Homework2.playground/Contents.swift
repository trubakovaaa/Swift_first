import Foundation

// Task 2.1
let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Task 2.2
//let milkPrice: Int = 3

// Task 2.3
let milkPrice: Double = 4.2

// Task 2.4
var milkBottleCount: Int? = nil
var profit: Double = 0.0

milkBottleCount = 20
profit = milkPrice * Double(milkBottleCount!)
print ("profit = ", profit)

// Task 2.4*
// Безопасное развертывание более корректное, т.к. учитывает, что переменная может быть не задана.
if let intMilkBottleCount = milkBottleCount {
    profit = milkPrice * Double(intMilkBottleCount)
    print ("profit = ", profit)
} else {
    print ("Bottles not found")
}

// Task 2.5
var employeesList = [String]()
employeesList += ["Иван", "Марфа", "Андрей", "Пётр", "Геннадий"]
print("Список сотрудников:", employeesList)

// Task 2.6
func checkWorkingHours(workingHours: Int) {
    print("Cотрудники отработали", workingHours, "часов")
    
    var isEveryoneWorkHard: Bool = false
    isEveryoneWorkHard = workingHours >= 40
    print("Сотрудники работают добросовестно? -", isEveryoneWorkHard ? "да" : "нет")
}

checkWorkingHours(workingHours: 30)
checkWorkingHours(workingHours: 40)
