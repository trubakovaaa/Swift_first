import Foundation

enum Device {
    case iPhone14Pro
    case iPhoneXR
    case iPadPro
     
    var name: String {
        switch self {
        case .iPhone14Pro: return "iPhone 14 Pro"
        case .iPhoneXR: return "iPhone XR"
        case .iPadPro: return "iPad Pro"
        }
    }
     
    var screenSize: CGSize {
        switch self {
        case .iPhone14Pro: return CGSize(width: 393, height: 852)
        case .iPhoneXR: return CGSize(width: 414, height: 896)
        case .iPadPro: return CGSize(width: 834, height: 1194)
        }
    }
     
    var screenDiagonal: Double {
        switch self {
        case .iPhone14Pro: return 6.1
        case .iPhoneXR: return 6.06
        case .iPadPro: return 11
        }
    }
     
    var scaleFactor: Int {
        switch self {
        case .iPhone14Pro:
            return 3
        case .iPhoneXR, .iPadPro:
            return 2
        }
    }
     
    func physicalSize() -> CGSize {
        CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}


struct Device2 {
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    init(name: String, screenSize: CGSize, screenDiagonal: Double, scaleFactor: Int) {
        self.name = name
        self.screenSize = screenSize
        self.screenDiagonal = screenDiagonal
        self.scaleFactor = scaleFactor
    }
    
    func physicalSize() -> CGSize {
        CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
    
    static var iPhone14Pro: Device2 {
        return Device2(name: "iPhone 14 Pro", screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    }
    
    static var iPhoneXR: Device2 {
        return Device2(name: "iPhone XR", screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    }
    
    static var iPadPro: Device2 {
        return Device2(name: "iPad Pro", screenSize: CGSize(width: 834, height: 1194), screenDiagonal: 11, scaleFactor: 2)
    }
}


struct Device3 {
    enum DeviceType {
        case iPhone14Pro
        case iPhoneXR
        case iPadPro
    }
    
    let type: DeviceType
    
    init(type: DeviceType) {
        self.type = type
    }
    
    static var iPhone14Pro: Device3 {
        return Device3(type: .iPhone14Pro)
    }
    
    static var iPhoneXR: Device3 {
        return Device3(type: .iPhoneXR)
    }
    
    static var iPadPro: Device3 {
        return Device3(type: .iPadPro)
    }
    
    var name: String {
        switch type {
        case .iPhone14Pro: return "iPhone 14 Pro"
        case .iPhoneXR: return "iPhone XR"
        case .iPadPro: return "iPad Pro"
        }
    }
     
    var screenSize: CGSize {
        switch type {
        case .iPhone14Pro: return CGSize(width: 393, height: 852)
        case .iPhoneXR: return CGSize(width: 414, height: 896)
        case .iPadPro: return CGSize(width: 834, height: 1194)
        }
    }
     
    var screenDiagonal: Double {
        switch type {
        case .iPhone14Pro: return 6.1
        case .iPhoneXR: return 6.06
        case .iPadPro: return 11
        }
    }
     
    var scaleFactor: Int {
        switch type {
        case .iPhone14Pro:
            return 3
        case .iPhoneXR, .iPadPro:
            return 2
        }
    }
    
    func physicalSize() -> CGSize {
        CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}


print("\(Device.iPhoneXR.name) -> \(Device.iPhoneXR.physicalSize())")
print("\(Device2.iPhoneXR.name) -> \(Device2.iPhoneXR.physicalSize())")
print("\(Device3.iPhoneXR.name) -> \(Device3.iPhoneXR.physicalSize())")
