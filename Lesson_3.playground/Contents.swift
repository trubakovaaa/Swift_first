import Foundation

// Task 3.3.1
func squareRootDown(number: Double) -> Double {
    return sqrt(number).rounded(.down)
}
print(squareRootDown(number: 6.8))

//Task 3.3.2
func sumEvenNumbers(numberOne: Int, numberTwo: Int) -> Int {
    return (numberOne % 2 == 0 ? numberOne : 0) + (numberTwo % 2 == 0 ? numberTwo : 0)
}
print(sumEvenNumbers(numberOne: 1, numberTwo: 10))

//Task 3.3.3
func printTemperature(_ temperatureCelsius: Int) {
    func temperatureConvertToKelvin(_ temperatureCelsius: Int) -> Double {
        return Double(temperatureCelsius) + 273.15
    }
    
    func temperatureConvertToFahrenheit(_ temperatureFahrenheit: Int) -> Double {
        return (1.8 * Double(temperatureCelsius) + 32.0)
    }
    print("Температура в Цельсиях: \(temperatureCelsius)")
    print("Температура в Кельвинах: \(temperatureConvertToKelvin(temperatureCelsius))")
    print("Температура в Фаренгейтах: \(temperatureConvertToFahrenheit(temperatureCelsius))")
}
printTemperature(10)

//Task 3.3.4
func sumMaxInt() -> Int64 {
    return Int64(UInt32.max) + Int64(UInt32.max)
}
print("Сумма двух максимальных целых чисел равна = \(sumMaxInt())")

//Task 3.3.5
func smallestEvenMultiple(_ number: Int) -> Int {
    return number % 2 == 0 ? number : 2 * number
}
print("Наименьшее положительное число кратное как двум, так и самому аргументу: \(smallestEvenMultiple(7)) ")

//Task 3.6.1
func isExistFour(_ arrNumbers: [Int]) -> Bool {
    for n in arrNumbers {
        if n == 4 {
            return true
        }
    }
    return false
}

if isExistFour([1, 2, 5, 9, 4, 6, 10, 4, 0]) {
    print("Есть")
}


//Task 3.6.2
func printArrayInLine(_ arrNumbers:[Int]) {
    var strWithTire = "-"
    for n in arrNumbers {
        strWithTire += String(n) + "-"
    }
    print(strWithTire)
}
printArrayInLine([1, 2, 3, 4, 5, 6, 7])


//Task 3.6.3
func whileLess50(_ N: Int) {
    var result = N
    var num = 0
    while result > 50 {
        result = result / 2
        num += 1
        print("\(num) итерация - \(result)")
    }
    print("Получилось число: \(result)")
    print ("Было пройдено \(num) итераций")
}

whileLess50(1000)

//Task 3.9.1
let helloUser = { (nameUser: String) -> Void in
    print("Hello, \(nameUser)!")
}

helloUser("Anna")

//Task 3.9.2
func getPrint(_ n: Int) -> () -> Void {
    if n > 0 {
        return {
            print("!")
        }
    } else {
        return {
            print ("!!")
        }
    }
}

var printout = getPrint(0)
printout()

//Task 3.9.3
func mathOperation(n: Int, operation: (Int) -> Int) -> Int {
    n + operation(n)
}

let pow3 = { (n: Int) -> Int in
    n * n * n
}

print("\(mathOperation(n: 3, operation: pow3))")
